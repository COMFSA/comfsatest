<?php
class Get_scores_model extends CI_Model
{

	public function get_scores($access_key,$Lid)
	{
		$this->load->database();

		date_default_timezone_set('Asia/Calcutta');
		$current_date = date("Y-m-j");
		
		$sql_account_details 		= 'select count(*), Aid from accounts where  access_key = ?';
		$sql_data_account_details 	= $this->db->query($sql_account_details, array($access_key));
		$data_sql_account_details 	= $sql_data_account_details->result_array();
		$aid 						= $data_sql_account_details[0]['Aid'];
		$count 						= $data_sql_account_details[0]['count(*)'];

		if($count > 0)
		{
			

			$sql_interaction_details 		= 'select * from interactions where  Lid = ?';
			$sql_data_interaction_details 	= $this->db->query($sql_interaction_details, array($Lid));
			$data_sql_interaction_details 	= $sql_data_interaction_details->result_array();
			$number_of_rows 				= $sql_data_interaction_details->num_rows();

			if ($number_of_rows <= 0 ) {
				$result_data = array();
				$result_data['msg'][0]['probability'] = "0";
				$result_data['msg'][0]['freshness'] = "0";
				$result_data['msg'][0]['overall'] = "0";
				return json_encode($result_data);
			}

			$value = 0;
			$client = "client";
			foreach ($data_sql_interaction_details as $row)
			{
				$interactions_values = $row['interactions_values'];

				if($interactions_values == "positive")
				{
					$value = $value + 1;
				}
			}
			
			$probability_data = $value/$number_of_rows*100;
			



			$sql_time_stamp				= 'select max(time_stamp), count(*) from interactions where  Lid = ? and conversation_by = ?';
			$sql_data_time_stamp_details 	= $this->db->query($sql_time_stamp, array($Lid,$client));
			$data_sql_time_stamp_details 	= $sql_data_time_stamp_details->result_array();
			
			$max_time_stamp					= $data_sql_time_stamp_details[0]['max(time_stamp)'];
			$count_client 					= $data_sql_time_stamp_details[0]['count(*)'];
			// echo $count_client;

			$max_date = "";
			if($count_client > 0)
			{	
				$max_date = $max_time_stamp;
			}
			else
			{
				$sql_time_stamp					= 'select min(time_stamp) from interactions where  Lid = ? ';
				$sql_data_time_stamp_details 	= $this->db->query($sql_time_stamp, array($Lid));
				$data_sql_time_stamp_details 	= $sql_data_time_stamp_details->result_array();			
				$min_time_stamp					= $data_sql_time_stamp_details[0]['min(time_stamp)'];
				$max_date 						= $min_time_stamp;
				
			}	



				$start_date		= strtotime($max_date);
				$end_date 		= strtotime($current_date);
				$result			= $end_date - $start_date;
				$diff_date 		= round($result / 86400);


				$date_value = 0;
				if ($diff_date <= 0) {
					$date_value = 0;
				} else {
					$date_value = $diff_date;
				}

				$freshness = (365 - $date_value) / 365 *100;
				

				$overall = ($probability + $freshness)/2;
				
				$probability_round = round($probability, 2);
				$freshness_round = round($freshness, 2);
				$overall_round = round($overall, 2);

				if($probability_round == 100)
				{
					$probability_round = 99;
				}
				if($probability_round == 0)
				{
					$probability_round = 1;
				}


			// echo $probability."<br />".$freshness."<br />".$overall;

			$result_data = array();
			$result_data['msg'][0]['probability'] = $probability_round;
			$result_data['msg'][0]['freshness'] = $freshness_round;
			$result_data['msg'][0]['overall'] = $overall_round;
			return json_encode($result_data);
		}
		else
		{
			$result_data = array();
			$result_data['msg'][0]['probability'] = "0";
			$result_data['msg'][0]['freshness'] = "0";
			$result_data['msg'][0]['overall'] = "0";
			return json_encode($result_data);
		}


		

	}	

}
?>