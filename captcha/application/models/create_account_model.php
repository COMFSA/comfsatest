<?php
class Create_account_model extends CI_Model
{

	public function create_account($name,$email,$password,$phone_no,$services, $title, $account_code)
	{
		$this->load->database();

		$sql_account_code_details 		= 'select count(*) from account_code where  code = ?';
		$sql_data_account_code_details 	= $this->db->query($sql_account_code_details, array($account_code));
		$data_sql_account_code_details 	= $sql_data_account_code_details->result_array();
		$account_code_count 				= $data_sql_account_code_details[0]['count(*)'];

		if($account_code_count == 1)
		{	

			$sql_account_details 		= 'select count(*) from accounts where  email = ?';
			$sql_data_account_details 	= $this->db->query($sql_account_details, array($email));
			$data_sql_account_details 	= $sql_data_account_details->result_array();
			$count_account 				= $data_sql_account_details[0]['count(*)'];


			
			if($count_account == 0)
			{


			date_default_timezone_set('Asia/Calcutta');
			// $time_stamp = date("M j Y / G:i:s");
			$today = date("MjYGis");
			$result = $name.$email.$phone_no.$today;
			$key = "abcdefghijklmnopqrstuvwx";//Generate key based on name and todays date
				    	# show key size use either 16, 24 or 32 byte keys for AES-128, 192
				    	# and 256 respectively

			$iv_size 		= mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			$iv 			= mcrypt_create_iv($iv_size, MCRYPT_RAND);
			$ciphertext 	= mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $result, MCRYPT_MODE_CBC, $iv);
			$ciphertext 	= $iv . $ciphertext;
			$access_key 	= base64_encode($ciphertext);


			$password_hash = SHA1($password);
			$license_date = date("Y-m-d");
			$expiry_date = date('Y-m-d', strtotime('+1 years'));
			// $access_owner_key = "";
			$role = "admin";
			$status = "active";
			$max_users = "1";
			$any_other_info = "";
			$photo_link = "https://res.cloudinary.com/duzxqldao/image/upload/v1477635917/md9gpwe6awmduu1imcwl.jpg";
			$introduction_text = "Welcome";
			$result1 = str_replace('@', "", $email);
			$public_access_key = str_replace('.', "", $result1);
			
			
			

			$Insert_account = $this->db->query("INSERT INTO `accounts`( `name`, `email`, `phone_no`, `password`, `license_date`, `expiry_date`, `role`, `status`, `access_key`, `access_owner_key`, `max_users`, `any_other_info`, `access_owner_id`, `services`,`title`,`photo_link`,`introduction_text`) VALUES ('$name','$email','$phone_no','$password_hash','$license_date','$expiry_date','$role','$status','$access_key','$access_key','$max_users',' ','','$services','$title','$photo_link','$introduction_text')");
			$access_owner_id = $this->db->insert_id();

			$Update_account = $this->db->query("UPDATE `accounts` SET `access_owner_id`='$access_owner_id' where email = '$email' ");

			$delete_account_code = $this->db->query("DELETE FROM `account_code` WHERE  code = '$account_code' ");
			$InsertPublicAccessKey = $this->db->query("INSERT INTO `account_public_keys`( `Aid`, `access_owner_id`, `public_key`) VALUES ('$access_owner_id','$access_owner_id','$public_access_key')");
			
			return "Account Created Successfully";
		}
		else
		{
			return "Account Already Exists";
		}
	}
	else
	{
		return "Code Doesn't Exists";
	}	



		

		
		
	
		
	}	

	
}
?>