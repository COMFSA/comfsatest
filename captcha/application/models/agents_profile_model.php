<?php
class Agents_profile_model extends CI_Model
{

	public function index($profile_id)
	{
		$this->load->database();
		$sql_session_id 		= 'select * from profiles where profile_id = ?';
		$sql_data_session_id 	= $this->db->query($sql_session_id, array($profile_id));
		$data_sql_session_id 	= $sql_data_session_id->result_array();
		return $data_sql_session_id;

		
	}

	public function testimonials($profile_id)
	{
		$this->load->database();
		$sql_session_id 		= 'select * from testimonials where pid = ?';
		$sql_data_session_id 	= $this->db->query($sql_session_id, array($profile_id));
		$data_sql_session_id 	= $sql_data_session_id->result_array();
		return $data_sql_session_id;

		
	}
}
?>