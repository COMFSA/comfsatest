<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>How the falling rupee can affect you</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
  <link href="https://fonts.googleapis.com/css?family=Domine" rel="stylesheet">
  
  <style type="text/css">
  body{
  font-family: 'Domine', serif;
  } 
    h1
  {
    color:#FF0000;
    font-size:24px;
    text-align:center;
  }
  
  h2
  {
    font-size:18px;
    font-weight:bold;
  }
  
  p{
    font-size:16px;
  }
  
  </style>
  
  </head>
  <body>
    
  
  <div class="container">
  
  <div class="row">
  
  <div class="col-md-2">&nbsp;</div>
  <div class="col-md-8">
  <img src="http://finmantra.in/images/thumbnails/rupee-fall.jpg" alt="Is Retirement Planning Must" class="img-responsive center-block" />  
  </div>
  <div class="col-md-2">&nbsp;</div>
  
  </div>
  
  <div class="row">
  <div class="col-md-2">&nbsp;</div>
  <div class="col-md-8">
  
  <h1>How the falling rupee can affect you</h1>
  <hr/>
  <p>The Rupee fall can not only hit the importers and exporters of the country but given a period of time can also begin to affect the common man and put a big hole in his pocket. As the rupee value comes close to 64.5, the common man can get hit in a number of ways.</p>
<h2>Monthly house hold bills</h2>
<p>Inflation as we all know has been pinching us for quite some time now. The rupee fall has made the hole in our pocket a little more bigger. India imports crude oil, medicines in large quantities and these have an indirect impact on our day to day lives. Apart from these there are consumer goods which are also termed as Fast Moving Consumer Goods (FMCG) such as detergents, shampoos and soaps which are going to become expensive. So your monthly household budget is bound to go high eventually. Edible oils which are also imported are likely to be costlier now.</p>
<h2>Jobs</h2>
<p>Industries which are dependent on imports are also affected as they need to increase the cost of production and operations. In order to equate the increase in import costs, companies will have to hire less or keep the salaries constant.</p>
<p>While the import dependent industries suffer, the companies which earn in dollars can make more gains. However, there could also be a global recessionary condition which might take an adverse toll.</p>
<h2>Vacations</h2>
<p>The falling rupee is bad news for itinerant Indians and vacationers to a foreign country. Air fares could go up by 3 to 5 percent followed by shopping which would also become expensive. You might also have to shell out more even for entertainment as they also could get costlier.</p>
<h2>Automobiles</h2>
<p>The depreciation of rupee has impacted the automobile sector in three ways. First, as some of the components are imported, the input costs of would also be raised. Second, some companies will have to pay higher royalty to foreign parent firms. Third, many have foreign currency loans in the form of external commercial borrowings and foreign currency convertible bonds. This will ultimately force all the companies to hike the prices of their vehicles.</p>
<h2>Education (Abroad)</h2>
<p>Students who have taken loan to fund their foreign education will have to grind their teeth. As loans are given to students in Indian rupees and they pay for their expenses in foreign currency, the cost of living and education would increase even more. This makes it difficult for the people who want to study abroad think twice before going ahead as this could be a strong concern for some people.</p>



  </div>
  <div class="col-md-2"></div>
  </div>
  
  
  
  

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!--Google Analytics Script-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41861527-1', 'finmantra.in');
  ga('send', 'pageview');

</script>
  </body>
</html>