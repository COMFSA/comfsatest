  <!DOCTYPE html>
  <html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta property="og:title"              content="Nomination and it's importance" />
    <meta property="og:type"               content="Article"/>
    <meta property="og:description"        content="A nominee is a person in whose name assets, securities and other investments are transferred. He/ she receives the benefit of the wealth in case of death of the nominator. A nomination means to have the right to receive the asset and not to own it. A nominee can manage the assets as per the nominator's wishes but cannot sell the assets unless he/ she is a legal heir."/>
    <meta property="og:image"              content="https://s3.ap-south-1.amazonaws.com/cdnstoragefinamntrablog/appoint-a-nominee-now.jpg"/>

    <meta name="postcard_heading"          content="Nomination and it's importance">
    <meta name="postcard_image"            content="https://s3.ap-south-1.amazonaws.com/cdnstoragefinamntrablog/appoint-a-nominee-now.jpg">
    <meta name="postcard_contents"         content="A nominee is a person in whose name assets, securities and other investments are transferred. He/ she receives the benefit of the wealth in case of death of the nominator. A nomination means to have the right to receive the asset and not to own it. A nominee can manage the assets as per the nominator's wishes but cannot sell the assets unless he/ she is a legal heir.">
    <meta name="postcard_opinion"          content="">
    <meta name="postcard_source"           content="Know more from the link below">

    <title>Nomination and its importance</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Domine" rel="stylesheet">
    
    <style type="text/css">
      body{
      font-family: 'Domine', serif;
      } 
        h1
      {
        color:#FF0000;
        font-size:24px;
        text-align:center;
      }
      
      h2
      {
        font-size:18px;
        font-weight:bold;
      }
      
      p{
        font-size:16px;
      }
    
    </style>
    
    </head>

    <body>
  
    <br />
    <div class="container">
    
    <div class="row">
    
    <div class="col-md-2">&nbsp;</div>
    <div class="col-md-8">
    <img src="https://s3.ap-south-1.amazonaws.com/cdnstoragefinamntrablog/appoint-a-nominee-now.jpg" alt="Nomination and its importance" class="img-responsive center-block" /> 
    </div>
    <div class="col-md-2">&nbsp;</div>
    
    </div>
    
    <div class="row">
    <div class="col-md-2">&nbsp;</div>
    <div class="col-md-8">
    
    <h1>Nomination and its importance</h1>
    <hr/>
    
    <p><p>A nominee is a person in whose name assets, securities and other investments are transferred. He/ she receives the benefit of the wealth in case of death of the nominator. A nomination means to have the right to receive the asset and not to own it. A nominee can manage the assets as per the nominator's wishes but cannot sell the assets unless he/ she is a legal heir.</p><p>The purpose of saving, investing and having life insurance is to ensure that your family is protected financially after your death. It is important that they can access these investments and insurance quickly. Without a nomination in your insurance policy, your insurance company is not obligated to release the policy money. Your family need to obtain a Grant of Probate or Letter of Administration or Distribution Order, which may take several years. Having a nominee leads to less complications at the time of claim and distribution of the assets after your death. You can make the legal heir the nominee to avoid tedious procedures.</p><p>For most investments and insurance policies, you can fill out a form while applying with the particulars of the nominee. Witnesses may be required in some cases.</p></p>
      
      
    </div>
    <div class="col-md-2"></div>
    </div>
    
    
    <div class="jumbotron">
    
    <p>This article is brought to you by</p>
    
    <div class="row">
    <div class="col-md-4"><img src="2410abcdefgh" alt="Profile Image" class="img-responsive"></div>
    <div class="col-md-8"><p>nameofowner<br/>
    <font size="3"><i>titleofowner</i></font></p></div>
    <div class="col-md-8"><p><font size="3"><i>introductionofowner</i></font></p></div>
    </div>
    
    <div class"row">
    <h3>Get in touch with me by filling the form below</h3>
    <hr/>
    <table class="table table-striped table-hover" align="center" >



  <tr>
  <td>Name</td>
  <td><input name="name" id="name" required="required" type="text" value="" ></td>
  </tr>

  <tr>
  <td >Email</td>
  <td><input name="email" id="email" required="required" type="email" value="" ></td>
  </tr>

  <tr>
  <td >Age</td>
  <td><input name="age" id="age" required="required" type="text" value="" ></td>
  </tr>


  <tr>
  <td >Date Of Birth</td>
  <td><input name="dob" id="dob" required="required" type="text" value=""></td>
  </tr>

  <tr>
  <td >Service you are looking for</td>
  <td><textarea rows="4"  id="service_type" name="service_type">
  </textarea>
  </td>
  </tr>


  <tr>
  <td >Phone Number</td>
  <td><input type="text" id="phone_no" name="phone_no" required="required" /></td>
  </tr>




  <input name="public_key" id="public_key" required="required" type="hidden" value="24101993abcdefghj">
  <input name="land_line" id="land_line" required="required" type="hidden" value="0">
  <input name="article_title" id="article_title" required="required" type="hidden" value="life insurance">





  <tr>
  <td></td>
  <td><input type="submit" name="submit" value="Get Advice" class="btn btn-raised btn-info" data-toggle="modal" data-target="#create_lead_modal"  onclick="create_lead()"/></td>
  </tr>

  </table>
    </div>
    
    </div>
    
  </div>

  <script type="text/javascript">
      
    function create_lead()
    {
    var post_name = document.getElementById('name').value;
    var post_email = document.getElementById('email').value;
    var post_age = document.getElementById('age').value;
    var post_dob = document.getElementById('dob').value;
    var post_service_type = document.getElementById('service_type').value;
      var post_phone_no = document.getElementById('phone_no').value;
      var post_public_key = document.getElementById('public_key').value;
      var post_land_line = document.getElementById('land_line').value;
    var post_article_title = document.getElementById('article_title').value;
    var create_lead  = 'http://finmantra.in/lead_magnet/index.php/Account_public_key_cntr/create_lead';

    
    if(post_age == "")
    {
      post_age = "NA";
    }

    if(post_dob == "")
    {
      post_dob = "NA";
    }

    if(post_land_line == "")
    {
      post_land_line = "NA";
    }

    if(post_service_type == "")
    {
      post_service_type = "NA";
    }

    if(post_name == "" || (post_email == "" && post_phone_no == "")   )
       {
        var result = "Please Fill All The Fields";
        document.getElementById('modal_body').innerHTML = result;

      
      }
    
    else
      {
        
      if(post_email == "")
    {
      post_email = "NA";
    }

    if(post_phone_no == "")
    {
      post_phone_no = "NA";
    }
        $.post(create_lead,
        {
        name: post_name,
        email: post_email,
        age: post_age,
        dob: post_dob,
        service_type: post_service_type,
        phone_no: post_phone_no,
        public_key: post_public_key,
      article_title: post_article_title,
      land_line: post_land_line
        
          
        },
        function(data,status){
          //window.alert(data);
      if (status == "success"){
              var result;
              var jsonData = JSON.parse(data);
              for (var i = 0; i < jsonData.msg.length; i++) {
                var counter = jsonData.msg[i];
                result = counter.message;

              }
        document.getElementById('modal_body').innerHTML = result;
      }

          
        });
      }  

  }
  </script>




  <!-- Boot Strap Modal  -->
  <div class="modal fade" id="create_lead_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Status</h4>
        </div>

        <div class="modal-body">
          <p id="modal_body"></p>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!--End Of Boot Strap Modal  -->
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>

  <!--Google Analytics Script-->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-41861527-1', 'finmantra.in');
    ga('send', 'pageview');

  </script>
    </body>
  </html>
