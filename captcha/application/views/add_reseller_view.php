<!DOCTYPE html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Collabri5 - Offshore Software Development | Outsourced Software Development</title>

       <?php include 'cssincludes.php'; ?>

       <!-- Page Style CSS -->
       <style type="text/css">
         .slide-bg
         {
            
            background-image: url("<?php echo base_url('public/img/banner_home_page.png'); ?>");
            background-repeat: repeat-x;
            background-position: center;
            background-size: cover;
         }

         .whitecolor
         {
           color:#ffffff;
           font-weight: 400;
           text-align: center;
           font-family: 'Lato', sans-serif;
         }

         .headingstyle
         {
          color:#03a9f4;
          font-family: 'Lato', sans-serif;
          font-weight: 400;
         }

          .headingstyle1
         {
          color:#fff;
          font-family: 'Lato', sans-serif;
          font-weight: 400;
         }

         .paragraph
         {
          font-family: 'Lato', sans-serif;
          font-weight: 400;
          font-size: 14px;
          text-align: justify;

         }

         .paragraphsmall
         {
          font-family: 'Lato', sans-serif;
          font-weight: 400;
          font-size: 12px;
          text-align: justify;

         }

         .panelheight
         {
          min-height:550px;
          height: auto;
         }

         .contactpage
         {
          background-color:#0c0c0c;
          min-height:175px;
          height:auto;
         }

         
       </style>
  </head>
  
  <body>
      <?php include 'header.php' ?>


       <table width="350" border="0" align="center" cellpadding="0" cellspacing="1">
<tr>
<!-- <td><form  method="post" action="<?php echo base_url(); ?>index.php/welcome/issue_license">
 -->
 <table width="100%" border="0" cellspacing="1" cellpadding="3">
<tr><td colspan="3"><strong>Issue License</strong></td></tr>


<!-- <input name="access_key" id="name" value = "<?php echo $this->input->cookie('cookie_set3',true);?>" required="required" type="hidden"> -->


<tr>
<td >Name</td>
<td width="6">:</td>
<td width="301"><input name="name" id="name" required="required" type="text" ></td>
</tr>

<tr>
<td >Email</td>
<td width="6">:</td>
<td width="301"><input name="email" id="email" required="required" type="text" ></td>
</tr>

<tr>
<td >password</td>
<td width="6">:</td>
<td width="301"><input name="password" id="password" required="required" type="password" ></td>
</tr>


<tr>
<td >phone</td>
<td width="6">:</td>
<td width="301"><input name="phone_number" id="phone_number" required="required" type="text" ></td>
</tr>


<tr>
<td >particulars</td>
<td width="6">:</td>
<td width="301"><input name="particulars" id="particulars" required="required" type="text" ></td>
</tr>


<tr>
<td >No Of License</td>
<td width="6">:</td>
<td width="301"><input name="no_of_license" id="no_of_license" required="required" type="text" ></td>
</tr>


<tr>
<td >Amount Paid</td>
<td width="6">:</td>
<td width="301"><input type="text" id="amount_paid" name="amount_paid" required="required" /></td>
</tr>

<tr>
<td >Reseller Code</td>
<td width="6">:</td>
<td width="301"><input name="code" id="code" required="required" type="text" ></td>
</tr>

<tr>
<td >Role</td>
<td width="6">:</td>
<td width="301"><input name="role" id="role" required="required" type="text" ></td>
</tr>


</table>
<tr>
<td width="301"><input type="submit" name="submit" class="btn btn-info btn-lg" data-toggle="modal" data-target="#add_reseller_account_modal" onclick="add_reseller_account()" value="submit" /></td>
</tr>

<!-- </form>  -->
<!-- </td> --> 
</tr>
</table>


<script type="text/javascript">
      
  function add_reseller_account()
    {
      var post_session_value = '<?php echo $session; ?>';
      var post_name = document.getElementById('name').value;
      var post_email = document.getElementById('email').value;
      var post_password = document.getElementById('password').value;
      var post_phone = document.getElementById('phone_number').value;
      var post_particulars = document.getElementById('particulars').value;
      var post_no_of_license = document.getElementById('no_of_license').value;
      var post_amount_paid = document.getElementById('amount_paid').value;
      var post_code = document.getElementById('code').value;
      var post_role = document.getElementById('role').value;


      var add_reseller_account  = '<?php echo base_url(); ?>' + "index.php/welcome/add_reseller_account" ;
     

      // window.alert(post_user_name+"--");

      $.post(add_reseller_account,
        {
          name: post_name,
          email: post_email,
          password: post_password,
          phone_number: post_phone,
          particulars: post_particulars,
          no_of_license: post_no_of_license,
          amount_paid: post_amount_paid,
          code: post_code,
          role: post_role,
          session_value:post_session_value
            
        },
        function(data,status){
              // window.alert(data);

            if (status == "success"){
                    var result;
                    var jsonData = JSON.parse(data);
                    for (var i = 0; i < jsonData.msg.length; i++) {
                        var counter = jsonData.msg[i];
                        result = counter.message;
                    }
                    document.getElementById('modal_body').innerHTML = result;
                    
          }

        
            
        });
    }

  </script>



<!-- Boot Strap Modal  -->
<div class="modal fade" id="add_reseller_account_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Status</h4>
            </div>

            <div class="modal-body">
              <p id="modal_body"></p>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      
    </div>
</div>
<!--End Of Boot Strap Modal  -->








        <?php 
        include 'footer.php';
        include 'scriptincludes.php'; ?>
  </body>
</html>