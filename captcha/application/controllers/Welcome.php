<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$paths = array 
(
  '/opt/local/bin',
  '/opt/local/bin/',
  '/usr/bin/mogrify',
  '/usr/bin/mogrify/',
  '/usr/bin/convert',
  '/etc/php5/apache2/conf.d/',                        // 20-imagick.ini
  '/etc/php5/apache2/conf.d/20-imagick.ini',
  '/etc/php/7.0/apache2/conf.d/20-imagick.ini', // extension=imagick.so
  '/etc/ImageMagick',
    '/etc/ImageMagick-6',
  '/etc/ImageMagick/',
  '/usr/include/php5/ext/imagick',
  '/var/lib/dpkg/info/php5-imagick.prerm'
);
$path = $paths['12']; // all 10 options failed :-(
echo '$path --> ' .$path .'<br />';

$this->load->library('image_lib');

$config['image_library'] = 'ImageMagick'; // $config['image_library']     = 'imagick';
$config['library_path']    = $path;

if ( ! $this->image_lib->resize())
{
   echo '<h1 style="color:red">' .$this->image_lib->display_errors() .'</h1>';
}
	}

	public function image_magic()
	{
		$this->load->library('image_magician');

		$paths = array (
			'/etc/php5/apache2/php.ini',
			'/etc/ImageMagick-6',
  			'/etc/php/7.0/apache2/conf.d/20-imagick.ini');
		$image_path = "/var/www/html/images";
		$this->image_magician->is_animated($paths);
	}


	public function image_magick()
	{
		$this->load->library('image_lib');

        /* Size 350px x 300px */
        $config['image_library'] = 'ImageMagick';
        $config['library_path'] = '/usr/bin/';
        $config['source_image'] = '/var/www/html/images/icon2.png';
        $config['new_image'] = '/var/www/html/images/iconn6.png';
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 350;
        $config['height'] = 300;

        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        // $this->upload->display_errors();
        $this->image_lib->resize();
	}



	public function upload()
	{

		
		try {
			

			
			
				$this->load->model('upload_model');
				$result['data'] = $this->upload_model->index();
				$this->load->view("viewfile",$result);

				

		} catch(Exception $e) {
			$userdata = array();	
			$userdata['msg'][0]['message'] = "Error";
			echo json_encode($userdata);
		}


	}


	public function download()
	{

		
		try {

			

			$this->load->database();
			$res=array(11,12);
			$id=$this->input->get('id');

			$sql	 		= 'select count(*), video from videos where id = ? ';
			$sql_count		= $this->db->query($sql, array($id));
			$data_sql 		= $sql_count->result_array();
			$count 			= $data_sql[0]['count(*)'];

			if ($count == "1") {
		    echo $data_sql[0]['video'];
			}
			

			// header('Content-Type: video/3gp');

			// echo'<video width="320" height="240" controls>
			// <source src="data:video/3gp;base64,'.base64_encode($db_file).'" type="video/mp4" >
			// </video>';
	
		} catch(Exception $e) {
			$userdata = array();	
			$userdata['msg'][0]['message'] = "Error";
			echo json_encode($userdata);
		}


	}


public function downloadfile()
    {
          	$this->load->database();			
			$this->db->trans_start();
			$sql	 		= 'select * from videos ';
			$sql_count		= $this->db->query($sql, array());
			$data_sql 		= $sql_count->result_array();
			$id_values = "";
			foreach($data_sql as $row)
			{
				$id_values.= $row['id'].",";
				
			}
			
			$data_id['users'] = substr_replace($id_values, "", -1);
		
            $this->load->view("viewfile",$data_id);
                                   
                     
    }


	public function products()
	{
		$this->load->database();
		$sql	 		= 'select * from videos where  id=11 ';
		$sql_count 		= $this->db->query($sql, array());
		$data_sql 		= $sql_count->result_array();
		$db_file 		= $data_sql[0]['video'];
		// header('Content-Type: video/3gp');
		print $db_file;
	}

	public function agent_profiles()
	{

		
		$profile_id = $this->input->get('profile_id');
		$this->load->model('agents_profile_model');
		$data['users'] = $this->agents_profile_model->index($profile_id);
		$this->load->view("agent_profile_view",$data);
	}

	public function testimonials()
	{

		
		$profile_id = $this->input->get('profile_id');
		$this->load->model('agents_profile_model');
		$data['users'] = $this->agents_profile_model->testimonials($profile_id);
		$this->load->view("testimonials_view",$data);
	}

	

	
}
?>
