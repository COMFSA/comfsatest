<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leads_crm extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		

	}

	public function host_check()
	{
		echo $_SERVER['HTTP_HOST'];

	}


	public function login()
	{
		try
		{

			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$this->load->model('login_model');
			$result = $this->login_model->login($email,$password);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function reset_password()
	{
		try
		{

			$email = $this->input->post('email');
			

			$this->load->model('reset_password_model');
			$result = $this->reset_password_model->reset_password($email);

			$user_data = array();
			$user_data['msg'][0]['message'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'][0]['message'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function change_password_of_reset_model()
	{
		try
		{

			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$code = $this->input->post('code');
			

			$this->load->model('change_password_of_reset_model');
			$result = $this->change_password_of_reset_model->change_password($email, $password, $code);

			$user_data = array();
			$user_data['msg'][0]['message'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'][0]['message'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function change_password()
	{
		try
		{

			$email = $this->input->post('email');
			$new_password = $this->input->post('new_password');

			$this->load->model('change_password_model');
			$result = $this->change_password_model->change_password($email,$new_password);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function create_executive()
	{
		try
		{
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$phone_no = $this->input->post('phone_no');
			$access_owner_key = $this->input->post('access_owner_key');
			
			
			$this->load->model('create_executive_model');
			$result = $this->create_executive_model->create_executive($name,$email,$password,$phone_no,$access_owner_key);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function create_account()
	{
		try
		{
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$phone_no = $this->input->post('phone_no');
			$services = $this->input->post('services');
			$title = $this->input->post('title');
			$account_code = $this->input->post('account_code');			
			
			
			$this->load->model('create_account_model');
			$result = $this->create_account_model->create_account($name,$email,$password,$phone_no,$services, $title, $account_code);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function create_lead()
	{
		try
		{
			$access_key = $this->input->post('access_key');
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$age = $this->input->post('age');
			$phone_no = $this->input->post('phone_no');
			$dob = $this->input->post('dob');
			$service_type = $this->input->post('service_type');
			$land_line = $this->input->post('land_line');
			
			
			
			
			$this->load->model('create_lead_model');
			$result = $this->create_lead_model->create_lead($access_key,$name,$email,$age,$phone_no,$dob,$service_type,$land_line);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function delete_lead()
	{
		try
		{
			$access_key = $this->input->post('access_key');
			$lid = $this->input->post('lid');
			
			$this->load->model('delete_lead_model');
			$result = $this->delete_lead_model->delete_lead($access_key,$lid);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}

	
	public function update_lead()
	{
		try
		{
			$access_key = $this->input->post('access_key');
			$lid = $this->input->post('lid');
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$age = $this->input->post('age');
			$phone_no = $this->input->post('phone_no');
			$dob = $this->input->post('dob');
			$service_type = $this->input->post('service_type');
			$land_line = $this->input->post('land_line');
			
			$this->load->model('update_lead_model');
			$result = $this->update_lead_model->update_lead($access_key,$lid,$name,$email,$age,$phone_no,$dob,$service_type,$land_line);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}



	public function get_leads_data()
	{
		try
		{
			
			$access_key = $this->input->post('access_key');

			$this->load->model('get_leads_data_model');
			$result = $this->get_leads_data_model->get_data($access_key);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}


	public function interactions()
	{
		try
		{
			
			$access_key = $this->input->post('access_key');
			$Lid = $this->input->post('Lid');
			$interaction_text = $this->input->post('interaction_text');
			$interaction_value = $this->input->post('interaction_value');
			$interaction_type = $this->input->post('interaction_type');
			$conversation_by = $this->input->post('conversation_by');


			$this->load->model('interactions_model');
			$result = $this->interactions_model->interactions($access_key,$Lid,$interaction_text,$interaction_value,$interaction_type,$conversation_by);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function get_scores()
	{
		try
		{
			
			$access_key = $this->input->post('access_key');
			$Lid = $this->input->post('Lid');

			$this->load->model('get_scores_model');
			$result = $this->get_scores_model->get_scores($access_key,$Lid);

			echo $result;
		}
		catch(Exception $e)
		{
			$result_data = array();
			$result_data['msg'][0]['probability'] = "Error";
			$result_data['msg'][0]['freshness'] = "Error";
			$result_data['msg'][0]['overall'] = "Error";
			echo json_encode($result_data);

		}
	}

	public function delete_interactions()
	{
		try
		{
			
			$interaction_id = $this->input->post('interaction_id');
			$access_key = $this->input->post('access_key');	

			$this->load->model('interactions_model');
			$result = $this->interactions_model->delete_interactions($interaction_id,$access_key);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);

			
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}


	public function update_interactions()
	{
		try
		{
			$access_key = $this->input->post('access_key');
			$Lid = $this->input->post('Lid');
			$interaction_id = $this->input->post('interaction_id');
			$interaction_text = $this->input->post('interaction_text');
			$interaction_value = $this->input->post('interaction_value');
			$interaction_type = $this->input->post('interaction_type');

			$this->load->model('interactions_model');
			$result = $this->interactions_model->update_interactions($access_key,$Lid,$interaction_id,$interaction_text,$interaction_value,$interaction_type);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);

			
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}	

	
	public function get_leads_interactions_data()
	{
		try
		{
			
			$access_key = $this->input->post('access_key');
			$Lid = $this->input->post('lid');

			$this->load->model('interactions_model');
			$result = $this->interactions_model->get_leads_interactions_data($access_key,$Lid);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function get_public_access_key()
	{
		try
		{
			
			$Aid = $this->input->post('Aid');
			

			$this->load->model('get_public_access_key_model');
			$result = $this->get_public_access_key_model->get_public_access_key($Aid);

			$user_data = array();
			$user_data['msg'][0]['message'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'][0]['message'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function get_profile_photo_link()
	{
		try
		{
			
			$Aid = $this->input->post('Aid');
			

			$this->load->model('get_profile_photo_link_model');
			$result = $this->get_profile_photo_link_model->get_profile_photo_link($Aid);

			$user_data = array();
			$user_data['msg'][0]['message'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'][0]['message'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function test1()
	{   

		$this->load->library('email'); 
		 $this->email->from('shiv@collabri5.com', 'Collabri5 - Enquiry');
 		 $this->email->to('akashgangatkar@gmail.com','Akash');
  		 $this->email->subject('Enquiry - '. 'hello');
  		 $this->email->message('akash'."<br/>". 'hai'); 
  		try{
    			$this->email->send();
    			echo json_encode("success");
  			}catch(Exception $e){
    			echo json_encode("failed");
  			}
		// $this->load->library('email'); // load email library
	 //    $this->email->from('jobsurfer007@gmail.com', 'sender name');
	 //    $this->email->to('akashgangatkar@gmail.com');
	 //    $this->email->subject('Your Subject');
	 //    $this->email->message('Your Message');
	    
	 //    if ($this->email->send())
	 //        echo "Mail Sent!";
	 //    else
	 //        echo $this->email->print_debugger();
	    
	}

	public function update_profile()
	{
		try
		{
			
			$access_key = $this->input->post('access_key');
			$services = $this->input->post('services');
			$title = $this->input->post('title');
			$phone_no = $this->input->post('phone_no');
			

			$this->load->model('update_profile_model');
			$result = $this->update_profile_model->update_profile($access_key, $services, $title, $phone_no);

			$user_data = array();
			$user_data['msg'][0]['message'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'][0]['message'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function trending_news()
	{
		try
		{
			
			$access_key = $this->input->post('access_key');
			
			

			$this->load->model('trending_news_model');
			$result = $this->trending_news_model->trending_news($access_key);

			$user_data = array();
			$user_data['msg'] = $result;
			echo json_encode($user_data);
		}
		catch(Exception $e)
		{
			$user_data = array();
			$user_data['msg'] = "Error";
			echo json_encode($user_data);

		}
	}

	public function send_mail() { 
          $this->load->library('email');

$this->email->initialize(array(
  'protocol' => 'smtp',
  'smtp_host' => 'smtp.gmail.com',
  'smtp_user' => 'jobsurfer007@example.com',
  'smtp_pass' => 'kabali007',
  'smtp_port' => 587,
  'crlf' => "\r\n",
  'newline' => "\r\n"
));

$this->email->from('jobsurfer007@example.com', 'Your Name');
$this->email->to('akashgangatkar@gmail.com');

$this->email->subject('Email Test');
$this->email->message('Testing the email class.');
$this->email->send();

echo $this->email->print_debugger();
      } 
}
?>
