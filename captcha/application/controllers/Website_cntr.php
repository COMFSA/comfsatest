<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website_cntr extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function index()
	{

		function encryptData($value){
		   $key = "abcdefghijklimno";
		   $text = $value;
		   $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		   $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		   $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv);
		   return $crypttext;
		}

		function decryptData($value){
		   $key = "abcdefghijklimno";
		   $crypttext = $value;
		   $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		   $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		   $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypttext, MCRYPT_MODE_ECB, $iv);
		   return trim($decrypttext);
		} 
		error_reporting(-1);
		$filename = "192.168.1.21s/encdec/txt.txt";
		$handle = fopen($filename, "r");
		$content = fread($handle, filesize($filename));


		$EncryptedData=encryptData($content);
		$DecryptedData=decryptData($EncryptedData);

	}

	public function template1()
	{

		$id = $this->input->post('id');

		$this->load->model('template1_model');
		$data['result'] = $this->template1_model->template1($id);

		$this->load->view('template1/contact_us', $data);
	}

	public function about_us()
	{

		

		$this->load->view('template1/about_us');
	}

	public function blog()
	{

		

		$this->load->view('template1/blog');
	}	
}
?>
